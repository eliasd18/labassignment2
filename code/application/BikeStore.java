//Elias Dif 2134380
package application;
import vehicles.Bicycle;
public class BikeStore 
{
    public static void main(String[] args) 
    {
        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("Specialized", 20, 45);
        bicycles[1] = new Bicycle("Giant", 15, 60);
        bicycles[2] = new Bicycle("Trek", 9, 50);
        bicycles[3] = new Bicycle("Merida", 30, 30);

        for (int i = 0; i < bicycles.length; i++) 
        {
            System.out.println(bicycles[i]);
        }
    }
}
